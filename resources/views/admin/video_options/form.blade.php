<div class="form-group">
  <label for="autoplay">Autoplay</label>
  <br />
   <?php $boolCheck = (@$data->autoplay) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('autoplay', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('autoplay', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'autoplay','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="loop">Loop</label>
  <br />
   <?php $boolCheck = (@$data->loop) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('loop', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('loop', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'loop','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="controls">Controls</label>
  <br />
   <?php $boolCheck = (@$data->controls) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('controls', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('controls', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'controls','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="mute">Mute</label>
  <br />
   <?php $boolCheck = (@$data->mute) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('mute', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('mute', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'mute','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="fullscreen">Fullscreen</label>
  <br />
   <?php $boolCheck = (@$data->fullscreen) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('fullscreen', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('fullscreen', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'fullscreen','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="frameborder">Frameborder</label>
  <br />
   <?php $boolCheck = (@$data->frameborder) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('frameborder', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('frameborder', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'frameborder','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="related_videos">Related_videos</label>
  <br />
   <?php $boolCheck = (@$data->related_videos) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('related_videos', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('related_videos', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'related_videos','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="gyroscope">Gyroscope</label>
  <br />
   <?php $boolCheck = (@$data->gyroscope) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('gyroscope', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('gyroscope', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'gyroscope','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="accelerometer">Accelerometer</label>
  <br />
   <?php $boolCheck = (@$data->accelerometer) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('accelerometer', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('accelerometer', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'accelerometer','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="picture">Picture</label>
  <br />
   <?php $boolCheck = (@$data->picture) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('picture', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('picture', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'picture','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="encrypt_media">Encrypt_media</label>
  <br />
   <?php $boolCheck = (@$data->encrypt_media) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('encrypt_media', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('encrypt_media', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'encrypt_media','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="start">Start</label>
  <br />
   <?php $boolCheck = (@$data->start) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('start', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('start', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'start','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="portrait">Portrait</label>
  <br />
   <?php $boolCheck = (@$data->portrait) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('portrait', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('portrait', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'portrait','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="title">Title</label>
  <br />
   <?php $boolCheck = (@$data->title) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('title', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('title', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'title','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group">
  <label for="byline">Byline</label>
  <br />
   <?php $boolCheck = (@$data->byline) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('byline', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('byline', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'byline','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div><div class="form-group clearfix">
	<a href="{{route('adminVideoOptions')}}" class="btn btn-default">Back</a>
	<button type="submit" class="btn btn-primary float-right">
		<i class="fa fa-check" aria-hidden="true"></i>
		Save
	</button>
</div>