(function(){
	var videoBanner = $('#video-banner-modal'); 
	var fileLibrary = $('.files');
	var txtYoutube = videoBanner.find('#txtYoutube');
	var btnYoutube = videoBanner.find('#btnYoutube');
	var txtVimeo = videoBanner.find('#txtVimeo');
	var preview = videoBanner.find('.preview');
	var checkbox = videoBanner.find('.check');
	var videoFrame = $(document).find('#videoFrame');
	var tglYoutube = videoBanner.find('#youtubeToggle');
	var tglLibrary = videoBanner.find('#libraryToggle');
	var tglVimeo = videoBanner.find('#vimeoToggle');
	var uncheckAll = videoBanner.find('#uncheck');
	var timeInput = videoBanner.find('#timeInput');
	var counter =1;
	var selectedVideo = videoBanner.find('#selectedVideo');
	var arrayCheckedAttr = [];
	var filterForVimeo = videoBanner.find('.remove-for-vimeo');
	var filterForYoutube = videoBanner.find('.remove-for-youtube');
	var filterForUpload = videoBanner.find('.remove-for-upload');
	var optionForm = videoBanner.find('#video-banner-form');
	var videoType = videoBanner.find('#video-type');
	var youtubeLibrary = videoBanner.find('.youtubeLibrary');
	var videoLibrary = videoBanner.find('.videoLibrary');
	var vimeoLibrary = videoBanner.find('.vimeoLibrary');
	var uploadSavedLibrary = videoBanner.find('.videoSavedLibrary');
	var frametagData = '';
	var arrVideoOption = ['autoplay', 'loop', 'controls', 'mute', 'related_videos', 'fullscreen', 'frameborder', 'gyroscope', 'accelerometer', 'picture', 'encrypt_media', 'start', 'portrait', 'title', 'byline'];
	var btnVideoSelect = videoBanner.find('#btnVideoSelect');
	var uploadSource = videoBanner.find('#uploadVideoSource');
	var deleteAsset = $(document).find('#delete-selected-video');
	var filterVimeo = ['.autoplay', '.loop', '.controls', '.mute', '.fullscreen', '.frameborder', '.encrypt', '.start', '.portrait', '.title', '.byline', '.start'];
	var filterYoutube = ['.autoplay', '.loop', '.controls', '.mute', '.related', '.fullscreen', '.frameborder', '.gyroscope', '.accelerometer', '.picture', '.encrypt', '.start', '.start'];
	var filterUpload = ['.autoplay', '.loop', '.controls', '.mute'];
	var optionClasses = ['.autoplay', '.loop', '.controls', '.mute', '.related', '.fullscreen', '.frameborder', '.gyroscope', '.accelerometer', '.picture', '.encrypt', '.start', '.portrait', '.title', '.byline', '.start'];
	var deleteVideo = videoBanner.find('#sumoVideoBannerDelete');
	var deleteForm = videoBanner.find('#video-banner-delete');
	videoBanner.find('.delete-btn').on('click', function() {
		videoBanner.find('.delete-btn').addClass('hide');
		videoBanner.find('.confirm-btn').removeClass('hide');
	});
	videoBanner.find('.confirm-btn a').on('click', function(e) {
		e.preventDefault();
		resetDeleteBtns();
	});
	videoBanner.find('.confirm-btn .confirm-delete-btn').on('click', function(e) {
		e.preventDefault();
		var url = deleteForm.attr('action');
        var type = deleteForm.attr('method');
		var data = new FormData(document.getElementById(deleteForm.attr('id')));
		submitForm(url, data, type);
		$('.video-options').addClass('hide');
		checkbox.prop('checked', false);
		resetCheckedAttr();
	});
	function resetDeleteBtns() {
		videoBanner.find('.delete-btn').removeClass('hide');
		videoBanner.find('.confirm-btn').addClass('hide');
	}
	// deleteForm.on('submit', function(e){
	// 	e.preventDefault();
	// 	var url = deleteForm.attr('action');
    //     var type = deleteForm.attr('method');
	// 	var data = new FormData(document.getElementById(deleteForm.attr('id')));
	// 	submitForm(url, data, type);
	// });
	btnVideoSelect.on('click', function(e){
		$('.sumo-asset-video-container').toggleClass('hide');
		$('.sumo-video').val(selectedVideo.val());
		if(selectedVideo.val()!=''){
			$('#sumo-video-asset-select').attr('src',selectedVideo.data('pic'));
		}
	});

	$(document).on('click','#delete-selected-video', function(e){
		e.preventDefault();
		selectedVideo.val('');
		$('.sumo-asset-video-container').addClass('hide');
	});
	optionForm.on('submit', function(e){
		e.preventDefault();
		if(videoFrame.attr('data-frame')=='vimeo'){
			frametagData = generateTag(arrayCheckedAttr, 'vimeo', txtYoutube.val());
		}
		else if(videoFrame.attr('data-frame')=='youtube'){
			frametagData = generateTag(arrayCheckedAttr, 'youtube', txtYoutube.val());
		}
		else{
			frametagData = generateTag(arrayCheckedAttr, 'upload', uploadSource);
		}

		var videoData = {
			'frametag' : frametagData,
			'type' : videoFrame.attr('data-frame'),
		}
		$('#dataframe').val(frametagData);
		var url = optionForm.attr('action');
        var type = optionForm.attr('method');
		var data = new FormData(document.getElementById(optionForm.attr('id')));
		// data['frametag'] = frametagData;
		
		submitForm(url, data, type);
	});
	
	
	txtYoutube.on('keypress',function(e) {
		if(e.which == 13) {
			counter =2;
			var test = $(document).find('#videoFrame');
			// 	$(this).attr('src', youtubeLink($(this).val())+'?version=3');
			// });
			$('.video-options').removeClass('hide');
			checkbox.prop('checked', false);
			resetCheckedAttr();
			test.attr('src', youtubeLink($(this).val())+'?version=3');
			filterForVimeo.css('display', 'block');
			filterForYoutube.css('display', 'none');
		}
	});
	btnYoutube.on("click", function(){
	});

	txtVimeo.on('keypress',function(e) {
		if(e.which == 13) {
			counter =1;
			$('.video-options').removeClass('hide');
			checkbox.prop('checked', false);
			resetCheckedAttr();
			videoFrame.attr('src', vimeoLink($(this).val()));
			filterForVimeo.css('display', 'none');
			filterForYoutube.css('display', 'block');
			fileLibrary.find('.file').removeClass('active');
		}
	});
	
	uncheckAll.on("click", function(){
		checkbox.prop('checked', false);
		resetCheckedAttr();
	});

	checkbox.on("click", function(){
		var vidframe= $(document).find('#videoFrame');
        if($(this).is(":checked")) {
			checkAttr($(this).attr('id'), counter, vidframe);
			arrayCheckedAttr.push($(this).attr('id'));
			counter+=1
		} 
		else{
			resetCheckedAttr();
			arrayCheckedAttr = arrayRemove(arrayCheckedAttr, $(this).attr('id'));
			$($(this).attr('id')).prop('checked', false);
			counter+=1;
			for(var i = 0; i<arrayCheckedAttr.length; i++){
				checkAttr(arrayCheckedAttr[i], counter, vidframe);
			}
		}
	});
	function arrayRemove(arr, value) {
		return arr.filter(function(ele){
			return ele != value;
		});
	 }
	function resetCheckedAttr(){
		// checkbox.prop('checked', false);
		if(videoFrame.attr('data-frame')=='youtube'){
			videoFrame.attr('src', youtubeLink(txtYoutube.val())+'?version=3');
		}
		else if(videoFrame.attr('data-frame')=='vimeo'){
			videoFrame.attr('src',vimeoLink(txtVimeo.val()));
		}
		else{
			// videoFrame.attr('src', uploadLink(txtYoutube.val())+'?version=3');
		}
		videoFrame.attr('allow',null);
		videoFrame.attr('frameborder','0');
		videoFrame.attr('allowfullscreen',null);
	}
	
	function checkAttr(element, c, frame){
		if($('#video-type').val()!='upload'){
			if(element =='autoplay'){
				stringConcat(c,frame);
				frame.attr('src', frame.attr('src')+'autoplay=1');
				frame.attr('allow','autoplay;');
				frame.play;
			}
			else if(element =='loop'){
				stringConcat(c,frame);
				frame.attr('src', frame.attr('src')+'loop=1');
			}
			else if(element =='controls'){
				stringConcat(c,frame);
				frame.attr('src', frame.attr('src')+'controls=0');
			}
			else if(element =='related_videos'){
				stringConcat(c,frame);
				frame.attr('src', frame.attr('src')+'rel=0');
			}
			else if(element =='mute'){
				stringConcat(c,frame);
				frame.attr('src', frame.attr('src')+'mute=1');
			}
			else if(element =='portrait'){
				stringConcat(c,frame);
				frame.attr('src', frame.attr('src')+'portrait=1');
			}
			else if(element =='title'){
				stringConcat(c,frame);
				frame.attr('src', frame.attr('src')+'title=1');
			}
			else if(element =='byline'){
				stringConcat(c,frame);
				frame.attr('src', frame.attr('src')+'byline=1');
			}
			else if(element =='fullscreen'){
				frame.attr('allowfullscreen','allowfullscreen');
			}
			else if(element =='frameborder'){
				frame.attr('frameborder', 1);
			}
			else if(element =='gyroscope'){
				frame.attr('allow', frame.attr('allow')+'gyroscope; ');
			}
			else if(element =='accelerometer'){
				frame.attr('allow', frame.attr('allow')+'accelerometer; ');
			}
			else if(element =='picture'){
				frame.attr('allow', frame.attr('allow')+'picture-in-picture; ');
			}
			else if(element =='encrypt_media'){
				frame.attr('allow', frame.attr('allow')+'encrypted-media; ');
			}
			else if(element == 'start'){
				if(videoFrame.attr('data-frame')=='youtube'){
					stringConcat(c,frame);
					frame.attr('src', frame.attr('src')+'start='+timeToSeconds(timeInput.val()));
				}
				else if(videoFrame.attr('data-frame')=='vimeo'){
					frame.attr('src', frame.attr('src')+'#t='+timeToSeconds(timeInput.val())+'s');
				}
				else{
					stringConcat(c,frame);
					frame.attr('src', frame.attr('src')+'start='+timeToSeconds(timeInput.val()));
				}
			}
		}
		else{
			var autoplay = "";
			var mute = "";
			var loop = "";
			var controls = "";
			if(element =='autoplay'){
				frame.attr('autoplay', true);
			}
		}
	}

	function timeToSeconds(time){
		var hms = time;
		var a = hms.split(':');
		var seconds = (+a[0]) * 60 + (+a[1]); 

		return seconds;
	}
	function stringConcat(c, frame){
		if(c == 1){
			frame.attr('src', frame.attr('src')+'?');
		}
		else{
			frame.attr('src', frame.attr('src')+'&');
		}
	}
	function generateTag(checkAttr, type, code){
		var tag ="";
		var allow="";
		var fullscreen = '';
		var frameborder = 0;
		var cnt = 1;
		if($('#video-type').val()!='upload'){
			for(var i=0; i<checkAttr.length; i++){
				if(cnt==1 && videoFrame.attr('data-frame')=='youtube'){
					tag+='?';
					cnt+=1;
				}
				else{
					cnt+=1;
				}
				if(checkAttr[i]=='autoplay'){
					if(cnt>1){
						tag+='&';
					}
					tag+='autoplay=1';
					allow+= 'autoplay;';
				}
				else if(checkAttr[i]=='loop'){
					if(cnt>1){
						tag+='&';
					}
					tag+='loop=1';
				}
				else if(checkAttr[i]=='controls'){
					if(cnt>1){
						tag+='&';
					}
					tag+='controls=0';
				}
				else if(checkAttr[i]=='mute'){
					if(cnt>1){
						tag+='&';
					}
					tag+='mute=1';
				}
				else if(checkAttr[i]=='related_videos'){
					if(cnt>1){
						tag+='&';
					}
					tag+='rel=0';
				}
				else if(checkAttr[i]=='fullscreen'){
					fullscreen= 'allowfullscreen = "allowfullscreen"'
				}
				else if(checkAttr[i]=='frameborder'){
					frameborder=1;
				}
				else if(checkAttr[i]=='gyroscope'){
					allow+='gyroscope;';
				}
				else if(checkAttr[i]=='picture'){
					allow+='picture-in-picture;';
				}
				else if(checkAttr[i]=='encrypt_media'){
					allow+='picture-in-picture;';
				}
				else if(checkAttr[i]=='start'){
					if(cnt>1 && videoFrame.attr('data-frame')!='vimeo'){
						tag+='&';
					}
					if(videoFrame.attr('data-frame')=='vimeo'){
						tag+='#';
					}
					tag+='start='+timeToSeconds(timeInput.val());
					if(videoFrame.attr('data-frame')=='vimeo'){
						tag+='s';
					}
				}
			}
		}
		else{
			for(var i=0; i<checkAttr.length; i++){
				if(checkAttr[i]=='autoplay'){
					tag+='autoplay ';
				}
				else if(checkAttr[i]=='mute'){
					tag+='mute ';
				}
				else if(checkAttr[i]=='controls'){
					tag+='controls ';
				}
				else if(checkAttr[i]=='loop'){
					tag+='loop ';
				}
			}
		}
		if(videoFrame.attr('data-frame')=='youtube'){
			return youtubeFrame(code, tag, allow, fullscreen, frameborder);
		}
		else if(videoFrame.attr('data-frame')=='vimeo'){
			return vimeoFrame(code, tag, allow, fullscreen, frameborder)
		}
		else{
			return uploadFrame(code, tag);
		}
	}

	function youtubeFrame(code, attr, allow, fullscreen, border){
		var tag = '<iframe width="100%" src="https://www.youtube.com/embed/'+code+attr+'" data-frame="youtube" id="videoFrame" allow="'+allow+'" '+fullscreen+' frameborder="'+border+'"></iframe>';
		return tag;
	}
	function vimeoFrame(code, attr, allow, fullscreen, border){
		var tag = '<iframe width="100%" src="https://player.vimeo.com/video/'+code+attr+'" data-frame="vimeo" id="videoFrame" allow="'+allow+'" '+fullscreen+' frameborder="'+border+'"></iframe>';
		return tag;
	}
	function uploadFrame(link, tags){
		
		var tag = '<video id="videoFrame" data-frame="upload" width="100%" '+tags+'>'+
		'<source src="'+$('#uploadVideoSource').val()+'">'+
		'Your browser does not support the video tag.'+
		  '</video>';
		  return tag;
	}
	function firstUploadFrame(link){
		var tag = '<video id="videoFrame" data-frame="upload" width="100%">'+
		'<source src="'+link+'">'+
		'Your browser does not support the video tag.'+
	  	'</video>';
		return tag;
	}
	function youtubeLink(code){
		var link = 'https://www.youtube.com/embed/'+code+'';
		return link;
	}
	function vimeoLink(code){
		var link = 'https://player.vimeo.com/video/'+code+'';
		return link;
	}
	function uploadLink(code){
		var link = code;
		return link;
	}

	tglYoutube.on("click", function(){
		videoFrame.attr('data-frame', 'youtube');
		videoType.val('youtube');
		$('#deleteTypeVideo').val('youtube');
	});
	tglVimeo.on("click", function(){
		videoFrame.attr('data-frame', 'vimeo');
		videoType.val('vimeo');
		$('#deleteTypeVideo').val('vimeo');
	});
	tglLibrary.on("click", function(){
		videoFrame.attr('data-frame', 'upload');
		videoType.val('upload');
		$('#deleteTypeVideo').val('upload');
	});

	function submitForm(url, data, type){
		$.ajax({
			type : type,
			url: url,
			data : data,
			cache: false,
			contentType: false,
			processData: false,
			success : function(data) {
				if(data.type=="vimeo"){
					$('#vimeoLibrary').empty().append(data.view);
				}
				else if(data.type=="youtube"){
					$('.youtubeLibrary').empty().append(data.view);
				}
				else{
					$('.videoSavedLibrary').empty().append(data.view)
				}
				bindFiles();
			},
			error : function(data, text, error) {
				
			}
		});
	}
	function getVideoList(url, library) {
		$.ajax({
			type : 'GET',
			url: url,
			data : '',
			dataType: 'json',
			success : function(data) {
				library.empty().append(data.view);
				
				bindFiles();
			},
			error : function(data) {
				console.log('error');
			}
		});
	};
	videoBanner.on('shown.bs.modal', function () {
		getVideoList(youtubeLibrary.data('url'), youtubeLibrary);
		getVideoList(vimeoLibrary.data('url'), vimeoLibrary);
		getVideoList(videoLibrary.data('url'), videoLibrary)
		getVideoList(uploadSavedLibrary.data('url'), uploadSavedLibrary)
	});
	function findActiveAsset() {
		videoBanner.find('.file').removeClass('active');
		
	}
	function checkBoxMarker(element){
		if(element.length>0){
			for(var i=0; i<arrVideoOption.length; i++){
				if(element[0][arrVideoOption[i]]==1){
					$('#'+arrVideoOption[i]).prop('checked', true);
				}
				if(arrVideoOption[i] == 'start' && element[0][arrVideoOption[i]] != '00:00'){
					$('#start').prop('checked', true);
					$('#timeInput').val(element[0][arrVideoOption[i]]);
				}
			}
		}
	}

	function bindFiles() {
		$(document).find('.file').each(function() {
			var file = $(this);
			file.unbind();
			file.on('click', function(e) {
				resetDeleteBtns();
				selectedVideo.val(file.data('id'));
				$('#deleteVideo').val(file.data('id'));
				checkbox.prop('checked', false);
				e.preventDefault();
				fileLibrary.find('.file').removeClass('active');
				$(this).addClass('active');
				a=1;
				$('.video-options').removeClass('hide');
				$('.video-preview iframe:first').remove();
				$('.video-preview video:first').remove();
				if(file.data('upload')=='asset-upload'){
					$('.video-preview').append(firstUploadFrame(file.data('source')));
					$('#uploadVideoSource').val(file.data('source'));
					$('#sumoVideoBannerDelete').addClass('hide');
				}
				else{
					checkBoxMarker(file.data('option'));
					$('.video-preview').append(file.data('tag'));
					$('#sumoVideoBannerDelete').removeClass('hide');
				}
				for(var i = 0; i<optionClasses.length; i++){
					$(optionClasses[i]).css('display', 'none');
				}
				if(file.data('vidtype')=='youtube'){
					for(var i = 0; i<filterYoutube.length; i++){
						$(filterYoutube[i]).css('display', 'block');
					}
				}
				else if(file.data('vidtype')=='vimeo'){
					for(var i = 0; i<filterVimeo.length; i++){
						$(filterVimeo[i]).css('display', 'block');
					}
				}
				else{
					for(var i = 0; i<filterUpload.length; i++){
						$(filterUpload[i]).css('display', 'block');
					}
				}
			})
		})
	};
	
})();