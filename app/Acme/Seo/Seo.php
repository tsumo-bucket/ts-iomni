<?php 

namespace App\Acme\Seo;

use Illuminate\Support\Collection;
use App\Option;
use App\Asset;
use App\Seo as AppSeo;

class Seo
{
  public function get($object)
  {
    $seo = $object->seo->first();
    $asset = Asset::findOrFail($seo->image);

    $response = [];
    $response['title'] = $seo->title;
    $response['description'] = $seo->description;
    $response['image'] = $asset->path;

    return $response;
  }

  public function title($value)
  {
    $option = Option::whereSlug('meta-title')->first();
    $response = ($value) ? $value : $option->value;

    return $response;
  }

  public function description($value)
  {
    $option = Option::whereSlug('meta-description')->first();
    $response = ($value) ? $value : $option->value;
    
    return $response;
  }

  public function image($value)
  {
    $option = Option::whereSlug('meta-image')->first();
    $response = asset(($value) ? $value : @$option->image->path);
    
    return $response;
  }

  public function seoSave($seoData,$receiver)
  {

      $seoData['title'] = isset($seoData['title']) ? $seoData['title'] : '';
      $seoData['description'] = isset($seoData['description']) ? $seoData['description'] : '';
      $seoData['image'] = isset($seoData['image']) ? $seoData['image'] : '';
      
      $seo = AppSeo::whereSeoable_id($seoData['seoable_id'])->whereSeoable_type($seoData['seoable_type'])->first();
      if (is_null($seo)) {
          $seo = new AppSeo;
      }
      $seo->title = $seoData['title'];
      $seo->description = $seoData['description'];
      $seo->image = $seoData['image'];
      $receiver->seo()->save($seo);

  }

}